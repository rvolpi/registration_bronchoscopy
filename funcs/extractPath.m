function bufferPoints = extractPath(checkedPoints,startPointIndex,endPointIndex)


% Extract the path between two points.
% Based on the labeled centerline, with bifurcation points.
% 
% INPUT:  Nx7 matrix -> N points with X,Y,Z coordinates, L and D1,D2,D3.
% OUTPUT: centerline points associated to the path chosen.

startPoint = checkedPoints(startPointIndex,:);
endPoint = checkedPoints(endPointIndex,:);

% I'm using -2 as an indication for already analyzed points.

checkedPoint(startPointIndex,7) = -2; 

refPoint = startPoint;

bufferPoints = startPoint;

while(1)
    
    if refPoint(1:3) == endPoint(1:3)
        break
    end
    
    % Avoiding points already checked.
    
    checkedPoints = checkedPoints(checkedPoints(:,7)~=-2,:);
    
    % Find the closest point to the reference one.
    
    [minimum minimumIndex] = min(pdist2(refPoint(1:3),checkedPoints(:,1:3)));
    
    % Go back to bifurcation point in case we are at one extremity.
    
    if minimum > 40
        bPts = find(bufferPoints(:,5)~=-1);
        refPoint = bufferPoints(bPts(end),:);
        bufferPoints = bufferPoints(1:bPts(end-1),:);
        continue
    end
    
    nextPoint = checkedPoints(minimumIndex,:);
    
    checkedPoints(minimumIndex,7) = -2;
    
    bufferPoints = [bufferPoints;refPoint];
    
    refPoint = nextPoint;
    
    
end




