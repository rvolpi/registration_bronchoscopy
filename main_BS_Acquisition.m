% Thesis pictures: z coordinate, reference value plotted, 
% stop pre-acquiring and transitions.

clear,clc

format shortg

sys = fSysSetup([0,1,4], 'session', 'portable');

% Buffer vectors are used to store data data acquired through the probe. 

probePoints1 = [];
probePoints2 = [];
probePoints3 = [];

carinaPoints = [];

counterKeysPressed = 0;

% First loop: trachea 1------------------------------------------------------------------------------------------------------------------------------------------------------------

carinaStop = stoploop('Press when the probe is touching the carina.');

while(~carinaStop.Stop()),       % Check if the loop has to be stopped


    % Acquiring one point from sensor 1, and storing coordinates in buffers.
        
    sys = fSysDAQUpdate(sys);
    [positionVector, positionMatrix] = fGetSensorPosition(sys,1);
    
    probePoints1 = [probePoints1;positionVector(1),positionVector(2),positionVector(3),1];
    
end

carinaPoints = [carinaPoints; probePoints1(end,1:3)];

carinaStop.Clear() ;  % Clear up the box
clear carinaStop ;    % this structure has no use anymore


% Second loop: mian bronchus 1 ------------------------------------------------------------------------------------------------------------------------------------------------------------

carinaStop = stoploop('Press when the probe is touching the carina.');

while(~carinaStop.Stop()),       % Check if the loop has to be stopped


    % Acquiring one point from sensor 1, and storing coordinates in buffers.
        
    sys = fSysDAQUpdate(sys);
    [positionVector, positionMatrix] = fGetSensorPosition(sys,1);
    
    probePoints2 = [probePoints2;positionVector(1),positionVector(2),positionVector(3),2];
    
end

carinaPoints = [carinaPoints; probePoints2(end,1:3)];

carinaStop.Clear() ;  % Clear up the box
clear carinaStop ;    % this structure has no use anymore


% Third loop: mian bronchus 3 ------------------------------------------------------------------------------------------------------------------------------------------------------------

carinaStop = stoploop('Press when the probe is touching the carina.');

while(~carinaStop.Stop()),       % Check if the loop has to be stopped


    % Acquiring one point from sensor 1, and storing coordinates in buffers.
        
    sys = fSysDAQUpdate(sys);
    [positionVector, positionMatrix] = fGetSensorPosition(sys,1);
    
    probePoints3 = [probePoints3;positionVector(1),positionVector(2),positionVector(3),3];
    
    
end

carinaPoints = [carinaPoints; probePoints3(end,1:3)];

carinaStop.Clear() ;  % Clear up the box
clear carinaStop ;    % this structure has no use anymore

% Fourth loop: trachea 2 ------------------------------------------------------------------------------------------------------------------------------------------------------------

carinaStop = stoploop('Press when balanced survey is finished.');

while(~carinaStop.Stop()),       % Check if the loop has to be stopped


    % Acquiring one point from sensor 1, and storing coordinates in buffers.
        
    sys = fSysDAQUpdate(sys);
    [positionVector, positionMatrix] = fGetSensorPosition(sys,1);
    
    probePoints1 = [probePoints1;positionVector(1),positionVector(2),positionVector(3),1];
    
    
end

carinaPoints = [carinaPoints; probePoints1(end,1:3)];

carinaStop.Clear() ;  % Clear up the box
clear carinaStop ;    % this structure has no use anymore


% ------------------------------------------------------------------------------------------------------------------------------------------------------------------------


probeCarinaPoint = mean(carinaPoints);
probePoints = [probePoints1; probePoints2; probePoints3];







