function [centerlinePoints, clCarinaPoint] = extractCenterline(checkedPoints)

outputFile = fopen('data.txt','w');
fprintf(outputFile, '%s','');
fclose(outputFile);

h = clickA3DPoint(checkedPoints(:,1:3)');

disp('Select, in this order: start point, carina point, left bronchus end, right bronchus end.')
disp(' ')
disp('Then, press any key to continue.')

pause;

indices = load('data.txt');

startIndex = indices(1);
carinaIndex = indices(2);
br1Index = indices(3);
br2Index = indices(4);

tic
path1 = extractPath(checkedPoints,startIndex,carinaIndex);
path2 = extractPath(checkedPoints,carinaIndex,br1Index);
path3 = extractPath(checkedPoints,carinaIndex,br2Index);
toc

% Plotting.

figure(1)
scatter3(path1(:,1),path1(:,2),path1(:,3),100,'.r')
hold on
grid off
scatter3(path2(:,1),path2(:,2),path2(:,3),100,'.g')
scatter3(path3(:,1),path3(:,2),path3(:,3),100,'.b')

centerlinePoints = [[path1(:,1:3),ones(length(path1),1)];[path2(:,1:3),2*ones(length(path2),1)];[path3(:,1:3),3*ones(length(path3),1)]];
clCarinaPoint = checkedPoints(carinaIndex,1:3);
    
