% Thesis pictures: z coordinate, reference value plotted, 
% stop pre-acquiring and transitions.

clear,clc,close all

format shortg

sys = fSysSetup([0,1,4], 'session', 'portable');

% Buffer vectors are used to store data data acquired through the probe. 

xbuffer = [];
ybuffer = [];
zbuffer = [];
ang1buffer = [];
ang2buffer = [];


% outOfTrack is a boolean used to detect transition from stable to unstable
% stete, and viceversa. 

outOfTrack = 0;

% checkSize is the number of last points considered to value movements.

checkSize = 20;

% First checkSize points: we record the standard deviation which will be
% used as a reference for further confrontation. We are assuming that the
% patient is making no strange movements for the first checkSize points.

figure(1)
% c = clock;
% disp(c(4:6));

pause(0.1)

% Just a set of points to have te time to get ready.

for i = 1:10
    disp(i)
    sys = fSysDAQUpdate(sys);
    [positionVector, positionMatrix] = fGetSensorPosition(sys,1);    
end

tic

% Defining the period in which data fr reference are acquired.

finalTime_refData = datenum(clock + [0,0,0,0,0,6]); % 6 seconds.

% Pre-processing: loop after which ref. values are extracted

while(datenum(clock) < finalTime_refData)

    % Acquiring one point from sensor 2, and storing coordinates in buffers.
    
    
    sys = fSysDAQUpdate(sys);
    [positionVector, positionMatrix] = fGetSensorPosition(sys,1);

    if (positionVector(3)>0.14 && positionVector(3)<0.16)
        positionVector(1)=xbuffer(end);
        positionVector(2)=ybuffer(end);
        positionVector(3)=zbuffer(end);
        positionVector(4)=ang1buffer(end);
        positionVector(5)=ang2buffer(end);
    end
    
    xbuffer = [xbuffer, positionVector(1)];
    ybuffer = [ybuffer, positionVector(2)];
    zbuffer = [zbuffer, positionVector(3)];
    ang1buffer = [ang1buffer, positionVector(4)];
    ang2buffer = [ang2buffer, positionVector(5)];
    
    % Storing the reference for the means. We do it in every loop, to visualize as well.      
    
    xMeanRef = mean(xbuffer);
    yMeanRef = mean(ybuffer);
    zMeanRef = mean(zbuffer);
    ang1MeanRef = mean(ang1buffer);
    ang2MeanRef = mean(ang2buffer);
    
    % Calculating the mean difference among consecutive points -> use take
    % into account X,Y,Z. The square is to avoid negative/positive issues.
    
%     refMeanDer = mean(diff(xbuffer.^2+ybuffer.^2+zbuffer.^2));

    refMeanDer = mean(diff(xbuffer).^2+diff(ybuffer).^2+diff(zbuffer).^2);

    figure(1)
    plot(zbuffer,'b')
 
    pause(0.000001)
    
end

save preProcTestData

toc
% c = clock;
% disp(c(4:6));
disp('Reference values stored!')
disp(refMeanDer)
disp(['MeanX = ',num2str(xMeanRef),', MeanY = ',num2str(yMeanRef),', MeanZ = ',num2str(zMeanRef),', MeanAng1 = ',num2str(ang1MeanRef),', MeanAng2 = ',num2str(ang2MeanRef)]);
disp('Acquiring...')

% We use newPositonsBuffer to store 200 values after the patient is stable again.
% We will calculate the mean of this set of points.
% switchedToStable is a boolean set to true when we are stable again.

newPositionsBuffer = [];
switchedToStable = 0;

% Indices of graph red-lines.

savedIndices = [length(zbuffer)];

% Store in keyValueBuffer the rapports.

keyValueBuffer = [];

finalTime = datenum(clock + [0,0,0,0,0,15]); % 20 seconds.

% Main loop: breathing session.

while(datenum(clock) < finalTime)
    
%     disp('----')
%     disp(abs(zMeanRef))
%     disp('')
%     disp(abs(mean(zbuffer(end-50:end))))
    
    % Constant checking of the mean, not only 
    % abrupt movements can cause a permanent shift!
  
    if(outOfTrack==0 && switchedToStable==0)
        if(abs(mean(xbuffer(end-50:end) - xMeanRef))>0.02 || abs(mean(zbuffer(end-50:end) - zMeanRef))>0.02)
            
            disp('---means updated!---')
            xMeanRef = mean(xbuffer(end-50:end));
            yMeanRef = mean(ybuffer(end-50:end));
            zMeanRef = mean(zbuffer(end-50:end));
            ang1MeanRef = mean(ang1buffer(end-50:end));
            ang2MeanRef = mean(ang2buffer(end-50:end));
        end
    end
          
    % Acquiring one point from sensor 2, and storing coordinates in buffers.
    
    sys = fSysDAQUpdate(sys);
    [positionVector, positionMatrix] = fGetSensorPosition(sys,1);

    % Sensor issue.
    
    if (positionVector(3)>0.14 && positionVector(3)<0.16)
        positionVector(1)=xbuffer(end);
        positionVector(2)=ybuffer(end);
        positionVector(3)=zbuffer(end);        
        positionVector(4)=ang1buffer(end);
        positionVector(5)=ang2buffer(end);
    end
    
    xbuffer = [xbuffer, positionVector(1)];
    ybuffer = [ybuffer, positionVector(2)];
    zbuffer = [zbuffer, positionVector(3)];
    ang1buffer = [ang1buffer, positionVector(4)];
    ang2buffer = [ang2buffer, positionVector(5)];
    
    figure(1)
    plot(zbuffer,'b')
    
    % In case we are buffering after unstable->stable transition.     
    
    if switchedToStable == 1
       
        newPositionsBuffer = [newPositionsBuffer;positionVector(1),positionVector(2),positionVector(3),positionVector(4),positionVector(5)];
        
%         disp(newPositionsBuffer)
        
        if size(newPositionsBuffer,1)>=50
            
            meanRefs = mean(newPositionsBuffer,1);
            
            xMeanOld = xMeanRef;
            yMeanOld = yMeanRef;
            zMeanOld = zMeanRef;
            ang1MeanOld = ang1MeanRef;
            ang2MeanOld = ang2MeanRef;
            
            xMeanRef = meanRefs(1);
            yMeanRef = meanRefs(2);
            zMeanRef = meanRefs(3);
            ang1MeanRef = meanRefs(4);
            ang2MeanRef = meanRefs(5);
            
            switchedToStable = 0;
            newPositionsBuffer = [];
            
            toc
            % c = clock;
            % disp(c(4:6));
            disp(['DeltaX = ',num2str(xMeanOld-xMeanRef),', DeltaY = ',num2str(yMeanOld-yMeanRef),', DeltaZ = ',num2str(zMeanOld-zMeanRef),', DeltaAng1 = ',num2str(ang1MeanOld-ang1MeanRef),', DeltaAng2 = ',num2str(ang2MeanOld-ang2MeanRef)]);
            savedIndices = [savedIndices,length(zbuffer)];
        
        end
        
    end
       
    % Calculating the mean difference.
    
    % Checking the mean difference of the last checkSize points: if is too 
    % high, we stop plotting green and red points. Also, is in th previous 
    % step we were stable, a warning string is displayed.

    meanDer = mean(diff(xbuffer(end-checkSize:end)).^2+diff(ybuffer(end-checkSize:end)).^2+diff(zbuffer(end-checkSize:end)).^2);

    keyValueBuffer = [keyValueBuffer, meanDer/refMeanDer];
    
    if meanDer > 3.0*refMeanDer
       
        if outOfTrack == 0
            % c = clock;
            % disp(c(4:6));
            toc
            disp('Unstable...');
            savedIndices = [savedIndices,length(zbuffer)];
            outOfTrack = 1;
            switchedToStable = 0;
            newPositionsBuffer = [];
        end
        
    else
        
        % If mean(diff(...)) is fine, we calculate the mean and plot all. 
        % Also, in case in the previous step we were unstable, the delta  
        % is calculated, and a notice string is displayed.   
        
        xMean = mean(xbuffer(end-checkSize:end));
        yMean = mean(ybuffer(end-checkSize:end));
        
        if outOfTrack == 1
            
            % c = clock;
            % disp(c(4:6));
            toc
            disp('Stable...');
            savedIndices = [savedIndices,length(zbuffer)];
            switchedToStable = 1;
            newPositionsBuffer = [];
            outOfTrack = 0;
        end
        
    end
    
    pause(0.000005)
    
end

save testData11

clear,clc,close all

load testData11

plot(zbuffer)

hold on

box on

x1 = savedIndices(1);
y1 = get(gca,'ylim');

figure(1)

plot([x1 x1], y1, 'g')

if(length(savedIndices)>1)
    
    x2 = savedIndices(2);
    x3 = savedIndices(3);
    x4 = savedIndices(4);

    plot([x2 x2], y1, 'r')
    plot([x3 x3], y1, 'g')
    plot([x4 x4], y1, 'g')

end









