% Run the system for a single sensor
% SETTINGS
trackingSensor = 1;
refSensor = 4;

N = 1000;
SlicerEnable = 1;
StorageEnable = 1;
CompensationEnable = 1;
transformName = 'ProbeToTracker';







%% Variables for positions
positionStorage1 = zeros(N, 5);
positionStorage2 = zeros(N, 5);
slicerStorage1 = zeros(4, 4, N);
slicerStorage2 = zeros(4, 4, N);

positionVector1 = [0,0,0,0,0];
positionVector2 = [0,0,0,0,0];

%% Enable Slicer
if(SlicerEnable == 1)
    slicerConnection = igtlConnect('127.0.0.1', 18944);
    transform.name = transformName;
end


sys = fSysSetup([0,trackingSensor,refSensor], 'session', 'portable');
% Give the DAQ time to startup;
pause(2);
sys = fSysDAQUpdate(sys);
[sys.refPos, sys.refMat] = fGetSensorPosition(sys, 2);

%sys.refPos =  fGetSensorPosition(sys, 2);

 
%% Main position tracking loop 
i = 1;
j = 1;
while(1)
   
   tic
   % Get new DAQ data
   sys = fSysDAQUpdate(sys);
   
   % Decode positions for both sensors. Set the estimate to the previous
   % result
   sys.estimateInit = positionVector1;
   positionVector1 = fGetSensorPosition(sys, 1);
   positionStorage1(i, :) = positionVector1;
   disp(positionVector1);
   
   sys.estimateInit = positionVector2;   
   positionVector2 = fGetSensorPosition(sys, 2);
   positionStorage2(i, :) = positionVector2;
   disp(positionVector2);
   
   
   if (i == N)
      save(strcat('savedpoints/S1points', num2str(j)), 'positionStorage1');
      save(strcat('savedpoints/S2points', num2str(j)), 'positionStorage2');
      positionStorage1 = zeros(N, 5);
      positionStorage2 = zeros(N, 5);
      
      i = 0;
      j = j + 1;
   end
   
   
   %% Extra processing goes here (before being sent to Slicer)
   % Riccardos work (breathing compensation and registration)
   % Alex registration correction (rigid patient movement and rotation)
   
 
   
   
   
    sys.SlicerT = [  -0.9647   -0.2496   -0.0836   24.1554;...
    0.2319   -0.6558   -0.7185  219.9579;...
    0.1245   -0.7125    0.6905 -111.1326;...
         0         0         0    1.0000]; 
   
    if CompensationEnable == 1
        %refOffset = (positionVector2 - sys.refPos);
        %refOffset(1:3) = refOffset(1:3) .* 1000 ;
        
        
        offsetT = sys.refMat \ fSphericalToMatrix(positionVector2);
        offsetT(1:3,4) = offsetT(1:3,4) * 1000;
        %offsetT = fSphericalToMatrix(refOffset);
        sys.SlicerT = offsetT * sys.SlicerT ;
    end
   
   
   
   if(SlicerEnable == 1)
       

      % Add pi for Slicer position.
      positionVector1(4) = positionVector1(4) + pi;
      % Convert meters to millimeters.
      positionVector1(1:3) = positionVector1(1:3) * 1000;
      % Convert from Spherical to Homogenous rotation matrix.
      positionMatrix = fSphericalToMatrix(positionVector1);
      
      
      transform.matrix = sys.SlicerT * positionMatrix;
      transform.timestamp = igtlTimestampNow();
      igtlSendTransform(slicerConnection, transform);
      
   end
   toc
   i = i + 1;
   pause(0.001);
   clc;
   
end


%% Save and cleanup



if(SlicerEnable == 1)
    igtlDisconnect(slicerConnection);
end