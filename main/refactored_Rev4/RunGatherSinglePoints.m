
num_iter=input('Number of points to collect: '); %set number of times to run position algorithm





regPoints = NaN(num_iter,3);



for i=1:num_iter;
   
    
    fprintf('Point %d...', i);
    pause;
    
    sys = fSysDAQUpdate(sys);
    
    solstore1 = fGetSensorPosition(sys, 1);
    
    regPoints(i,:) = solstore1(1:3)
      
    
    fprintf('Done\n');
    pause(0.5);
    
end
save('data/regPoints', 'regPoints')