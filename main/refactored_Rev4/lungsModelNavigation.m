sys = fSysSetup([0,1,4], 'legacy', 'portable');

% Buffer vectors are used to store data data acquired through the probe. 

xbuffer = [];
ybuffer = [];
zbuffer = [];

% outOfTrack is a boolean used to detect transition from stable to unstable
% stete, and viceversa. 

outOfTrack = 0;

% checkSize is the number of points among which meanswill be calculated.

checkSize = 50;

% First checkSize points: we record the standard deviation which will be
% used as a reference for further confrontation. We are assuming that the
% patient is making no strange movements for the first checkSize points.

figure(1)
sys = fSysSensor(sys,1);

while(1)
    
    
    sys = fSysDAQUpdate(sys);
    [positionVector1, positionMatrix1] = fGetSensorPosition(sys);
    

    xbuffer = [xbuffer, positionVector1(1)];
    ybuffer = [ybuffer, positionVector1(2)];
    zbuffer = [zbuffer, positionVector1(3)];
    
    scatter3(xbuffer,ybuffer,zbuffer,10,'.r');
    
    pause(0.01)
    
end
