function [bufferDirections, pointToSlicer] = getCenterLinePoint(roughPoint,bufferDirections)

if size(bufferDirections,1) < 10
    
    % For the first ten points.
    
    [minimum,idx] = min(pdist2(roughPoint,centerLinePoints(:,1:3)));
    pointToSlicer = centerLinePoints(idx,1:3);
    newDirs = centerLinePoints(idx,4:end);
    bufferDirections{end+1} = newDirs(newDirs~=-1);
    
else

    % We take the nearest centerline points, among the ones allowed by
    % bufferDirections.

    pointsToEval = centerLinePoints(ismember(centerLinePoints(:,4),[bufferDirections{:}]),:);
    [minimum,idx] = min(pdist2(roughPoint,pointsToEval(:,1:3)));
    pointToSlicer = pointsToEval(idx,1:3);

    % Updating bufferDirections with new point's label and allowed directions.
    % We should consider carefully the buffer thing, right now is very
    % rudimentary.

    newDirs = pointsToEval(idx,4:end);
    bufferDirections{end+1} = newDirs(newDirs~=-1);
    bufferDirections = bufferDirections(2:end);

end