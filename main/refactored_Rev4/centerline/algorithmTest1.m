sys = fSysSetup([0,1,4], 'legacy', 'portable');

bufferDirections = {[]};
roughPositions = [];
centerLinePositions = [];

while(1)
    
    sys = fSysSensor(sys,1);
    [positionVector1, positionMatrix1] = fGetSensorPosition(sys);
    
    [bufferDirections, pointToSlicer] = getCenterLinePoint(positionVector1(1:3),bufferDirections);
    
    % Last two lines to check results at the end.     
    
    roughPositions = [roughPositions; positionVector1(1:3)];
    centerLinePositions = [centerLinePositions; pointToSlicer]; 
   
end