function dataListen(src, event)
% Global variable to store session sample data from the DAQ
global sessionData

% Assign new data to the global variable for use in fDAQAcquire
sessionData = event.Data;

end