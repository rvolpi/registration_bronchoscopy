function NIDAQ = fDAQSetup(sampleFreq, channels, DAQType, numSamples)

if (strcmpi(DAQType, 'LEGACY'))
    % Old legacy interface for the NI DAQ. Only compatable with 32bit
    % Matlab
    if (~isempty(daqfind)) %checks if daq card is detected
        stop(daqfind)
    end

    NIDAQ = analoginput('nidaq','Dev1'); %define the analog input
    set(NIDAQ,'InputType','SingleEnded') %set the inputs to be single ended not differential
    addchannel(NIDAQ, channels); %specifies the input channel, 12 is the output of the instrumentation amplifier and 15 is the output of he current summing circuit (I think)
    set(NIDAQ, 'SampleRate', sampleFreq); %sets the sample rate

    set(NIDAQ, 'TriggerType', 'immediate'); %triggers sampling immediately
    set(NIDAQ, 'SamplesPerTrigger', Inf); %samples contiously after initialisation
    set(NIDAQ, 'TriggerRepeat', 0); % number of times to repeat trigger
    
    
elseif (strcmpi(DAQType, 'SESSION')) 
    % Experiment to test the new NI DAQ interface for MATLAB
    
    % Fixes clock synchronisation issue with DAQmx 14.0
    daq.reset
    daq.HardwareInfo.getInstance('DisableReferenceClockSynchronization',true);
    
    
    NIDAQ = daq.createSession('ni');

    ch1 = addAnalogInputChannel(NIDAQ,'Dev1', channels(1), 'Voltage');
    ch1.TerminalConfig = 'SingleEnded';
    ch2 = addAnalogInputChannel(NIDAQ,'Dev1', channels(2), 'Voltage');
    ch2.TerminalConfig = 'SingleEnded';
    if(length(channels) == 3)
        ch3 = addAnalogInputChannel(NIDAQ,'Dev1', channels(3), 'Voltage');
        ch3.TerminalConfig = 'SingleEnded';
    end



    NIDAQ.Rate = sampleFreq;
    NIDAQ.NumberOfScans = numSamples;
    NIDAQ.NotifyWhenDataAvailableExceeds = numSamples;
    
    NIDAQ.IsContinuous = true;
    myListen = @(src, event)dataListen(src, event);
    lh = addlistener(NIDAQ,'DataAvailable', myListen);
    NIDAQ.startBackground(); 

end


end