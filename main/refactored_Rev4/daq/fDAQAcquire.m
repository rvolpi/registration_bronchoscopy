function rawData = fDAQAcquire(NIDAQ, DAQType, tVector)

global sessionData
timeDelay=.001;

if strcmpi(DAQType, 'legacy')
    isrunning(NIDAQ); %checks if the DAQ is responding
    %if the DAQ runs into memory trouble, we stop and start the acquisition and reset everything
    if isrunning(NIDAQ) == 0; 
        stop(NIDAQ);
        flushdata(NIDAQ);
        start(NIDAQ);
        pause(.5);
        %memory_probs=memory_probs+1;
    end
    pause(timeDelay);

    rawData = peekdata(NIDAQ,length(tVector)); %Grab data from DAQ


elseif strcmpi(DAQType, 'session')

    % rawData takes the value of the global sessionData variable
    rawData = sessionData;


else
    error('Please specify a interface type (legacy or session)');
    
end

end