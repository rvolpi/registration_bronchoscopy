
num_iter=input('Number of points to collect:\n '); %set number of times to run position algorithm
minmaxOption = input('Enter 1 for inhalation. Enter 0 for exhalation:\n');




regPoints = NaN(num_iter,3);

pointBufferS1 = zeros(100,5);
pointBufferS2 = zeros(100,1);

for i=1:num_iter;
   
    pointBufferS1 = zeros(100,5);
    pointBufferS2 = zeros(100,1);
    
    fprintf('Point %d...', i);
    pause;
    
    
    for j = 1:100
        
        j
        
        sys = fSysDAQUpdate(sys);

        sys = fSysSensor(sys,1);
        pos1 = fGetSensorPosition(sys);
    
        sys = fSysSensor(sys,2);
        pos2 = fGetSensorPosition(sys);

        pointBufferS1(j,:) = pos1; 
        pointBufferS2(j,:) = pos2(3);
      
    end
    
    if minmaxOption == 1
        [maxumum, mIndex] = max(pointBufferS2);
    elseif minmaxOption == 0
        [minimum, mIndex] = min(pointBufferS2);   
    end
    
    localMaxPoint = pointBufferS1(mIndex,:);
    
    figure(i)
    
    plot(pointBufferS2,'b');
    hold on
    plot(mIndex,pointBufferS2(mIndex),'or');
    
    regPoints(i,:) = localMaxPoint(1:3);
    fprintf('Done\n');
    pause(0.5);
    
end
save('data/regPoints', 'regPoints')