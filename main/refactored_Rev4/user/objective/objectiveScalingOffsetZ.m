function [Bdiff]=objectiveScalingOffsetZ(parameters, calFieldCoil, coilIndex, sys)


%The first three parameters add an offset to the x,y,z parameters 

numCalPoints = 49;

x=sys.xtestpoint;
y=sys.ytestpoint;
z=sys.ztestpoint-parameters(1); % add offset

%% Calculate magnetic field at each point

for i=1:numCalPoints;

            [Hx(i,:),Hy(i,:),Hz(i,:)]= spiralCoilFieldCalcMatrix(1,sys.xcoil(coilIndex,:),sys.ycoil(coilIndex,:),sys.zcoil(coilIndex,:),x(i),y(i),z(i));

end
%%

Bz = sys.u0*Hz*parameters(2); %scale result

%vectorise each component
BVectorised = (sys.Iout(coilIndex)'./sys.Rstore(coilIndex)').*Bz;

Bdiff = (calFieldCoil - BVectorised');
