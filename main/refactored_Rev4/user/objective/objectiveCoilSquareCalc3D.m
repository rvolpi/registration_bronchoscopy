function [out]= objectiveCoilSquareCalc3D(currentPandO, sys, fluxReal)



x = currentPandO(1);
y = currentPandO(2);
z = currentPandO(3);
theta = currentPandO(4);
phi = currentPandO(5);


[Hx,Hy,Hz]= spiralCoilFieldCalcMatrix(1,sys.xcoil,sys.ycoil,sys.zcoil,x,y,z); %calculates magnetic field componenets


Bx=sys.u0.*Hx;
By=sys.u0.*Hy;
Bz=sys.u0.*Hz;

%model calculation of flux
fluxModel=(sys.BScaleActive)'.*(sys.Iout'./sys.Rstore').*(Bx.*sin(theta).*cos(phi)+By.*sin(theta).*sin(phi)+Bz.*cos(theta));

out = fluxModel - fluxReal; %calculate difference between the model and the measurement