function fFigureUpdate(sysParams, currentPandO, figureHandles)
    
    % Length of the arrow.
    len = .06;
    
    %FIXES ANGLE FOR DISPLAY.
    currentPandO(4)=currentPandO(4) + pi; %fix angle for display purposes
    %determine start and end point of the sensor position arrow
    

    if strcmpi(sysParams.model, 'portable') == 1
        arrow_start=[-currentPandO(1)+.5*len*sin(currentPandO(4))*cos(currentPandO(5)),currentPandO(2)-.5*len*sin(currentPandO(4))*sin(currentPandO(5)),currentPandO(3)+.5*len*cos(currentPandO(4))];
        arrow_stop=[-currentPandO(1)-.5*len*sin(currentPandO(4))*cos(currentPandO(5)),currentPandO(2)+.5*len*sin(currentPandO(4))*sin(currentPandO(5)),currentPandO(3)-.5*len*cos(currentPandO(4))];

    elseif strcmpi(sysParams.model, 'fixed') == 1
        arrow_start=[-currentPandO(1)+.5*len*sin(currentPandO(4))*cos(currentPandO(5)),currentPandO(2)-.5*len*sin(currentPandO(4))*sin(currentPandO(5)),-currentPandO(3)+.5*len*cos(currentPandO(4))];
        arrow_stop=[-currentPandO(1)-.5*len*sin(currentPandO(4))*cos(currentPandO(5)),currentPandO(2)+.5*len*sin(currentPandO(4))*sin(currentPandO(5)),-currentPandO(3)-.5*len*cos(currentPandO(4))];

    else
    error('Please specify the system model ("portable" or "fixed")');
    end
    
    
    
    %draw the arrow line
    set(figureHandles.h1,'X',[arrow_start(1) arrow_stop(1)],'Y',[arrow_start(2) arrow_stop(2)],'Z',[arrow_start(3) arrow_stop(3)]);

    %draw dot
    set(figureHandles.h2,'XData',[arrow_stop(1)],'YData',[arrow_stop(2)],'ZData',[arrow_stop(3)]);

    %update the sensor positoin in the textbox
    set(figureHandles.h3,'String',{strcat('x=',num2str(currentPandO(1),3)),strcat('y=',num2str(currentPandO(2),3)),strcat('z=',num2str(currentPandO(3),3))});


end