function figureHandles = fFigureSetup()

    figure(1);

    
    set(gca,'FontSize',20,'FontWeight','demi','FontName','Times') %set axis font
    h1=plot3(nan,nan,nan,'MarkerSize',50,'LineWidth',7,'Color',[1 0 0]); %sets handle to draw line on display for sensor orientation
    hold on
    h2 = scatter3(nan,nan,nan,'MarkerFaceColor',[1 0 0],'MarkerEdgeColor',[0 0 0],'SizeData',200); %handle for displaying a dot at the sensor position
    h3 = annotation('textbox',[0.75 0.69 0.1 0.11],'FontSize',20,'BackgroundColor',[1 1 1],'String', 'Test'); %handle to display position in text box
    grid on %turns on matlab grid

    axis([-.15 .15 -.15 .15 0 .3]) %sets axis of the display
    xlabel('X [m]');
    ylabel('Y [m]');
    zlabel('Z [m]');

    figureHandles.h1=h1;
    figureHandles.h2=h2;
    figureHandles.h3=h3;
end