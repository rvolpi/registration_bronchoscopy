function rmsErrorBz = fCheck(sys)



% Define position algorithm parameters
options = optimset('TolFun',1e-17,'TolX',1e-8,'MaxFunEvals',500,'MaxIter',500,'Display','final');


% Specify initial conditions, should be exact
% xFixed = sysParams.xtestpoint;
% yFixed = sysParams.ytestpoint;
% thetaFixed = 0;
% phiFixed = 0;


solution = zeros(length(sys.ztestpoint),5); %specify output store variable

for i =1:length(sys.ztestpoint) % go through each test point and calculate position
    
    x0 = [sys.xtestpoint(i),sys.ytestpoint(i), sys.ztestpoint(i), 0, 0];
    fluxReal = sys.BStoreActive(i,:)';
	        
	
    
    objectiveCoil3D = @(currentPandO)objectiveCoilSquareCalc3D(currentPandO, sys, fluxReal);
    
    
    % Run the algorithm depending on the setup configuration. This should
    % be handled in a  different way at some point.
    if strcmpi(sys.model, 'portable') == 1
        solution(i,:)= lsqnonlin(objectiveCoil3D,x0,[-.25 -.25 0 -pi -2*pi],[.25 .25 0.5 pi 2*pi],options);     
    elseif strcmpi(sys.model, 'fixed') == 1
        solution(i,:)= lsqnonlin(objectiveCoil3D,x0,[-.25 -.25 -0.3 -pi -2*pi],[.25 .25 0 pi 2*pi],options);
    else
        error('Please select model of system');
    end
    

end


%determine position error
xError=(sys.xtestpoint' - solution(:,1));
yError=(sys.ytestpoint' - solution(:,2));
zError=(sys.ztestpoint' - solution(:,3));

rError = sqrt(xError.^2+yError.^2+zError.^2); %calculate rms error
rmsErrorBz = 1000*mean(rError); %calculate mean rms error in mm


end