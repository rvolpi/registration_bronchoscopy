% Gathers 49 B-field points for system calibration
% INPUT:    sysParams: Stucture containing system settings
% OUTPUT:   Bstore: Returns array of 49x8 array of B field values. The 
% column index is the coil number. Each row comprises of the 8 received 
% B-field strength values from the respective coil at a known point of the
% duplo board
function sys = fGetCalField(sys)

% 7x7 grid of points on the Duplo board.
numPoints = 49;
for i=1:numPoints
    fprintf('Point %d.....', i);
    pause;
    sys = fSysDAQUpdate(sys);
    sys = fSysGetField(sys);
    sys.BStoreActive(i, :) = sys.BField;
    fprintf('Done\n');
end
fprintf('Point acquisition completed');



% Save the field data to its own file corrensponding to the sensor number.
BStore = sys.BStoreActive;
save(strcat('data/BStore',num2str(sys.SensorNo)), 'BStore');
% Halt the DAQ
stop(sys.NIDAQ);
fSysSave(sys);

end