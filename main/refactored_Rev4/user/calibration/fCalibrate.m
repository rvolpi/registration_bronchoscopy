% fCalibrate.m
% Determines the calibration parameters for the system
% INPUT:    
%           sys - A structure containing the system settings 
%           (dimensions, constants, frequencies etc)
% 
% OUTPUT:   BScale - An array of scaling factors for each coil
%           zOffsetAverage - The offset in the z-axis that increases
%           accuracy (NOTE THIS)
function sys = fCalibrate(sys)

numCoils = 8;

loader = load(strcat('data/BStore', num2str(sys.SensorNo)));
sys.BStoreActive = loader.BStore;


paraEst=[0 1]; %define initial estimate of parameters, first one is sensor offset and second one is scaling factor
options = optimset('TolFun',1e-22,'TolX',1e-7,'MaxFunEvals',2000,'MaxIter',2000); %define least squares algorithm parameters
zOffsetAllCoils = [0,0,0,0,0,0,0,0];

for coilNo=1:numCoils
	calFieldCoil=sys.BStoreActive(:,coilNo)'; %work though each coil
	%% Run algorithm
    scalingOffsetZ = @(params)objectiveScalingOffsetZ(params, calFieldCoil, coilNo, sys);
    
	[temp, resnorm] = lsqnonlin(scalingOffsetZ, paraEst,[],[],options); %a contains minimised parameters
	zOffsetAllCoils(coilNo) = temp(1); %store results
    
    sys.BScaleActive(coilNo) = temp(2);
end

% Gets the mean z-offset from the coils.
sys.zOffsetActive = mean(zOffsetAllCoils);

% Save calibration values to file CalibrationDataSensorX where X is the
% selected sensor.
zOffset = sys.zOffsetActive
BScale = sys.BScaleActive
save(strcat('data/Calibration', num2str(sys.SensorNo)), 'zOffset', 'BScale');

sys = fSysSave(sys);

end