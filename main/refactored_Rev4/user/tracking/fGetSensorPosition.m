function [positionVector, positionMatrix] = fGetSensorPosition(sys, sensorNo)
%FGETPOINT Decodes the detected magnetic field by the selected sensors.
%   Detailed explanation goes here

sys = fSysSensor(sys, sensorNo);

if sys.SensorNo == -1
    error('Please select the active sensor using fSysSensor()');
end


sys = fSysGetField(sys);

positionVector = fSysDecodeField(sys);
positionMatrix = fSphericalToMatrix(positionVector);

end

