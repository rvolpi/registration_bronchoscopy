function solution = fSysDecodeField(sys)

objectiveCoil3D = @(currentPandO)objectiveCoilSquareCalc3D(currentPandO, sys, sys.BField);
    
    if strcmpi(sys.model, 'portable')
        lowerbound = [-.25, -.25, 0, -2*pi, -2*pi];
        upperbound = [.25, .25, 0.5, 2*pi, 2*pi];
    elseif strcmpi(sys.model, 'fixed')
        lowerbound = [-.25, -.25, -0.5, -2*pi, -2*pi];
        upperbound = [.25, .25, 0, 2*pi, 2*pi];
    else
        error('Please specify the hardware setup being used') 
    end

    [solution,resnorm_store]= lsqnonlin(objectiveCoil3D, sys.estimateInit,lowerbound,upperbound,sys.lqOptions); % calls the least squares algorithm
    if resnorm_store>sys.residualThresh; % check if the residual is small enough
        % try again with a different initial condition, the angles are different here in the initial estimate
        [solution,resnorm_store]= lsqnonlin(objectiveCoil3D, sys.estimateInit+[0, 0, 0, pi, pi],lowerbound,upperbound,sys.lqOptions); 
        if resnorm_store>sys.residualThresh; % if it still fails, give up and use the intial estimate as the solution
            solution=sys.estimateInit;
        end
    end
end