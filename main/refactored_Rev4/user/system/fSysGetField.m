function sys = fSysGetField(sys)

% Increase the index by 1 due to the first column of data containing the
% DAQ data
dataColumn = sys.SensorNo + 1;


numCoils = 8;

X(:,1) = sys.rawData(:,1);
X(:,2) = sys.rawData(:, dataColumn);

X = X';
Y=(X.*sys.G)*sys.E; %Calculate each frequency component using this matrix fir method
MagY=2*abs(Y); %calculate the amplitude of each component, both current and magnetic field measurements are in here
PhaseY=angle(Y); %calculate the phase of each



if strcmpi(sys.model, 'portable') == 1
    Phase1 = PhaseY(1,:)-PhaseY(2,:)-sys.DAQPhase; %calculate the phase between the current and the magnetic field, subtracting the DAQ phase offset
elseif strcmpi(sys.model, 'fixed') == 1
    Phase1 = PhaseY(2,:)-PhaseY(1,:)-sys.DAQPhase;
end



%this for loops corrects the phase measurement, sometimes, randomly the
%phase changes by pi radiens, this loop ensures consistency, the phase
%should be between -pi and pi
for j=1:numCoils;
     if abs(Phase1(j))>pi

     Phase1(j)=-sign(Phase1(j))*(2*pi-abs(Phase1(j)));

     end
end



% Taking the sign of the phase difference and the magnetic field amplitude and a scaling factor, the magnetic field is determined 
if strcmpi(sys.model, 'portable')
   BField = sign(Phase1)'.*MagY(2,:)'./sys.Asens';
elseif strcmpi(sys.model, 'fixed')
   BField = sign(Phase1)'.*MagY(1,:)'./sys.Asens';
end
sys.BField = BField;
end