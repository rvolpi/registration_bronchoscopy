function result = fSystemTest( ...
sysParams,          ...
int_Niterations,	...
str_filename,       ...
bool_igtEnable,     ...
bool_figEnable      ) 

%% Setup
N = int_Niterations;
%delay = 0.001;

if exist('data/CalibrationData.mat', 'file') == 2
    load data/CalibrationData.mat;
    systemParams.BScale = BScale;
    systemParams.zOffset = zOffsetAverage;
else
    error('CalibrationData.mat does not exist. Calibration required');
end


if bool_igtEnable
    slicerConnection = fSlicerSetup('127.0.0.1', 18944);
    transform.name = 'probeToTracker';
end






if bool_figEnable
    figureHandles = fFigureSetup();
end


%% Position Tracking
SlicerPosition = zeros(4,4,N);
result = zeros(N,5);
for i=1:N;


    rawData = fDAQAcquire(sysParams);
    
    Bfield1 = fGetField(sysParams, rawData, 1); %stores the flux measurements in a global variable so the other fucntions can see it
    solstore1 = fDecodeField(Bfield1,sysParams);
    
    if length(sysParams.DAQchannels) == 3
        Bfield2 = fGetField(sysParams, rawData, 2);
        solstore2 = fDecodeField(Bfield2,sysParams);
    end
    
    
    
    
    
    if bool_figEnable
        fFigureUpdate(sysParams, solstore1, figureHandles);
    end
    
    
%     if i == 1
%         q = 1; %variable for the MA filter
%     end
% 
%     %update the moving average filter
%     sysParams.MAStore(q,:)=solstore;
%     sysParams.estimateInit = sum(sysParams.MAStore,1)/sysParams.MALength;
%     q = q + 1;
%     if q > sysParams.MALength; %resets counter after the length of the filter
%         q = 1;
%     end
%     

% New MA filter code layout
    for n = (sysParams.MALength - 1): -1 :1
        
        sysParams.MAStore(n+1,:) = sysParams.MAStore(n,:);
        
        if(n == 1)
            sysParams.MAStore(n,:) = solstore1;
        end
    end
    
    sysParams.estimateInit = sum(sysParams.MAStore)/length(sysParams.MAStore);
    
    
    
    
    fprintf('Iteration %d',i);
    r = sysParams.estimateInit
    
    % Random Correction from Kilians Code!
    solstore1(4) = solstore1(4) + pi;
    positionMatrix = fSphericalToMatrix(solstore1);
    
    result(i,:) = solstore1;
    
    if bool_igtEnable
        
        positionMatrix(:,4) = [positionMatrix(1,4), positionMatrix(2,4), positionMatrix(3,4), 0.001]' * 1000;
        
        
        transform.matrix = sysParams.SlicerT * positionMatrix;
        
        

        transform.timestamp = igtlTimestampNow();
        igtlSendTransform(slicerConnection, transform);
        
        SlicerPosition(:,:,i) = transform.matrix;
    end
    
    
    
    
    
    
    
    
    
end


if bool_igtEnable
    fSlicerClose(slicerConnection);
end

if strcmpi(sysParams.DAQType, 'legacy') == 1
    stop(sysParams.NIDAQ);
end

if bool_figEnable == 1
    hold off
end



end