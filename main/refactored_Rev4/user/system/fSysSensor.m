function sys = fSysSensor(sys, sensorNo )
%FSWITCHSENSOR Summary of this function goes here
%   Detailed explanation goes here
switch sensorNo
    case 1
        sys.SensorNo = 1;
        sys.zOffsetActive = sys.zOffset1;
        sys.BScaleActive = sys.BScale1;
        sys.BStoreActive = sys.BStore1;
         
    case 2
        sys.SensorNo = 2;
        sys.zOffsetActive = sys.zOffset2;
        sys.BScaleActive = sys.BScale2;
        sys.BStoreActive = sys.BStore2;
end

