function sys = fSysSave(sys)
%FSAVESYS Summary of this function goes here
%   Detailed explanation goes here
switch sys.SensorNo
    
    case 1
        sys.zOffset1 = sys.zOffsetActive;
        sys.BStore1 = sys.BStoreActive;
        sys.BScale1 = sys.BScaleActive;
    case 2
        sys.zOffset2 = sys.zOffsetActive;
        sys.BStore2 = sys.BStoreActive;
        sys.BScale2 = sys.BScaleActive;
end

save('data/sys', 'sys');

end

