function transform = fSlicerSend(connection, positionMatrix)


transform.name = 'probeToTracker';
transform.matrix = positionMatrix;
transform.timestamp = igtlTimestampNow();
igtlSendTransform(connection, transform);

end