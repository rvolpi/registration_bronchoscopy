
function varargout = GUI1(varargin)
%GUI1 M-file for GUI1.fig
%      GUI1, by itself, creates a new GUI1 or raises the existing
%      singleton*.
%
%      H = GUI1 returns the handle to a new GUI1 or the handle to
%      the existing singleton*.
%
%      GUI1('Property','Value',...) creates a new GUI1 using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to gui_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      GUI1('CALLBACK') and GUI1('CALLBACK',hObject,...) call the
%      local function named CALLBACK in GUI1.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI1

% Last Modified by GUIDE v2.5 06-May-2015 15:16:53

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gui_OpeningFcn, ...
                   'gui_OutputFcn',  @gui_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before GUI1 is made visible.
function gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

%build the logo:
% axes(handles.axes3);
% logo = imread('logo-ucc.jpg');
% image(logo);

logo = imread('img/logo-ucc.jpg', 'jpg');
image(logo, 'parent', handles.axes3);
axis(handles.axes3, 'off')

logo1 = imread('img/logo2', 'jpg');
image(logo1, 'parent', handles.axes5);
axis(handles.axes5, 'off')

% Choose default command line output for GUI1
handles.output = hObject;



%declarations
%settings

settings.ch1enable = 0;
settings.ch2enable = 0;
settings.ch3enable = 0;

settings.ch1 = '0';
settings.ch2 = '0';
settings.ch3 = '0';

settings.type= 'legacy';
settings.model= 'portable';
settings.sessionData = 0;

handles.settings = settings;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes GUI1 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = gui_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes during object creation, after setting all properties.
function axes2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes2


% --- Executes during object creation, after setting all properties.
function axes3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes3


% --- Executes on selection change in channel1choice.
function channel1choice_Callback(hObject, eventdata, handles)
% hObject    handle to channel1choice (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

 contents = cellstr(get(hObject,'String'));
 channel1 = contents{get(hObject,'Value')};
 text=channel1;
 set(handles.display , 'String', text);
 handles.settings.ch1 = channel1;
 guidata(hObject, handles);
% Hints: contents = cellstr(get(hObject,'String')) returns channel1choice contents as cell array
%        contents{get(hObject,'Value')} returns selected item from channel1choice


% --- Executes during object creation, after setting all properties.
function channel1choice_CreateFcn(hObject, eventdata, handles)
% hObject    handle to channel1choice (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in channel2choice.
function channel2choice_Callback(hObject, eventdata, handles)
% hObject    handle to channel2choice (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns channel2choice contents as cell array
%        contents{get(hObject,'Value')} returns selected item from channel2choice
contents = cellstr(get(hObject,'String'));
channel2 = contents{get(hObject,'Value')};
text=channel2;
set(handles.display , 'String', text);
handles.settings.ch2 = channel2;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function channel2choice_CreateFcn(hObject, eventdata, handles)
% hObject    handle to channel2choice (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in channel3choice.
function channel3choice_Callback(hObject, eventdata, handles)
% hObject    handle to channel3choice (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns channel3choice contents as cell array
%        contents{get(hObject,'Value')} returns selected item from channel3choice
contents = cellstr(get(hObject,'String'));
channel3 = contents{get(hObject,'Value')};
text=channel3;
set(handles.display , 'String', text);
handles.settings.ch3 = channel3;
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function channel3choice_CreateFcn(hObject, eventdata, handles)
% hObject    handle to channel3choice (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in modelchoice.
function modelchoice_Callback(hObject, eventdata, handles)
% hObject    handle to modelchoice (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns modelchoice contents as cell array
%        contents{get(hObject,'Value')} returns selected item from modelchoice
contents = cellstr(get(hObject,'String'));
model=contents{get(hObject,'Value')};
text=model;
set(handles.display , 'String', text);
handles.settings.model=model;
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function modelchoice_CreateFcn(hObject, eventdata, handles)
% hObject    handle to modelchoice (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in typechoice.
function typechoice_Callback(hObject, eventdata, handles)
% hObject    handle to typechoice (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns typechoice contents as cell array
%        contents{get(hObject,'Value')} returns selected item from typechoice
contents = cellstr(get(hObject,'String'));
type=contents{get(hObject,'Value')};
text=type;
set(handles.display , 'String', text);
handles.settings.type = type;
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function typechoice_CreateFcn(hObject, eventdata, handles)
% hObject    handle to typechoice (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in start.
function start_Callback(hObject, eventdata, handles)
% hObject    handle to start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

ch1 = str2num(handles.settings.ch1);
ch2 = str2num(handles.settings.ch2);
ch3 = str2num(handles.settings.ch3);

ch1enable = handles.settings.ch1enable;
ch2enable = handles.settings.ch2enable;
ch3enable = handles.settings.ch3enable;

model = handles.settings.model;
type = handles.settings.type;

if (ch1 ~= 0)
    error('Current sense must be 0');
end







 if(ch1enable && ch2enable)
     channels = [ch1, ch2];
     handles.settings.channels = channels;
     Setup = get (hObject,'value');
    if Setup 
        set(handles.Testbox,'visible','on');
    else
        set(handles.Testbox,'visible','off');
    end
   
    text='START with 2 channels';
    set(handles.display , 'String', text);
 elseif(ch1enable && ch2enable && ch3enable)
    channels = [ch1, ch2, ch3];
    handles.settings.channels = channels;
    Setup = get (hObject,'value');
    if Setup 
        set(handles.Testbox,'visible','on');
    else
        set(handles.Testbox,'visible','off');
    end
  
    text='START with 3 channels';
    set(handles.display , 'String', text);

 end

 handles.settings.sys = fSysSetup(channels, type, model);
 disp('Setup complete');
 
 
 
 

% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over start.
function start_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%sys



function Niterationchoice_Callback(hObject, eventdata, handles)
% hObject    handle to Niterationchoice (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Niterationchoice as text
%        str2double(get(hObject,'String')) returns contents of Niterationchoice as a double

text= str2double(get(hObject,'String'));
set(handles.display , 'String', text);


% --- Executes during object creation, after setting all properties.
function Niterationchoice_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Niterationchoice (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in figbox.
function figbox_Callback(hObject, eventdata, handles)
% hObject    handle to figbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of figbox
reste=mod(get(hObject,'Value'),2);
if reste==1
text='fig on';
set(handles.display , 'String', text);
else
text='fig off';
set(handles.display , 'String', text);

end

%print graph
if reste==1
hold on
axes(handles.axes2)
% plot(sin(0:.1:10));
[U,V] = meshgrid(0:.2:2*pi, 0:.2:2);
X = V.*cos(U);
 Y = V.*sin(U);
 Z = 2*U;
 surf(X,Y,Z)
%  figdown=gcf;

axes(handles.axes4)
[X,Y] = meshgrid(-10:0.25:10,-10:0.25:10);
f = sinc(sqrt((X/pi).^2+(Y/pi).^2));
surf(X,Y,f);
axis([-10 10 -10 10 -0.3 1])
xlabel('{\bfx}')
ylabel('{\bfy}')
 zlabel('{\bfsinc} ({\bfR})')
%  figup=gcf;


else
axes(handles.axes2);
cla;
axes(handles.axes4);
cla;
end

% --- Executes on button press in igtbox.
function igtbox_Callback(hObject, eventdata, handles)
% hObject    handle to igtbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of igtbox
reste=mod(get(hObject,'Value'),2);
if reste==1
text='igt on';
set(handles.display , 'String', text);
else
text='igt off';
set(handles.display , 'String', text);
end

% --- Executes on button press in Testchoice.
function Testchoice_Callback(hObject, eventdata, handles)
% hObject    handle to Testchoice (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
text='TEST';
set(handles.display , 'String', text);

sys=fSetup( handles.settings.channels , type, model);
handles.sys=sys;


% --- Executes on button press in ch1box.
function ch1box_Callback(hObject, eventdata, handles)
% hObject    handle to ch1box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
reste=mod(get(hObject,'Value'),2);
if reste==1
    text='Current sense ON';
    set(handles.display , 'String', text);
else
    text='Current sense OFF';
    set(handles.display , 'String', text);
end
handles.settings.ch1enable = get(hObject,'Value');

%visible
if reste==1  
    set(handles.channel1choice,'visible','on');
else
    set(handles.channel1choice,'visible','off');
end

guidata(hObject,handles);
% Hint: get(hObject,'Value') returns toggle state of ch1box


% --- Executes on button press in ch2box.
function ch2box_Callback(hObject, eventdata, handles)
% hObject    handle to ch2box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ch2box
reste=mod(get(hObject,'Value'),2);
if reste==1
text='Sensor 1 ON';
set(handles.display , 'String', text);
else
text='Sensor 1 OFF';
set(handles.display , 'String', text);
end

handles.settings.ch2enable = get(hObject,'Value');

%visible
if reste==1  
    set(handles.channel2choice,'visible','on');
else
    set(handles.channel2choice,'visible','off');
end

guidata(hObject,handles);

% --- Executes on button press in ch3box.
function ch3box_Callback(hObject, eventdata, handles)
% hObject    handle to ch3box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ch3box
reste=mod(get(hObject,'Value'),2);
if reste==1
text='Sensor 2 ON';
set(handles.display , 'String', text);
else
text='Sensor 2 OFF';
set(handles.display , 'String', text);
end

handles.settings.ch3enable = get(hObject,'Value');

%visible
if reste==1  
    set(handles.channel3choice,'visible','on');
else
    set(handles.channel3choice,'visible','off');
end

guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function axes4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes4


% --- Executes on button press in clear.
function clear_Callback(hObject, eventdata, handles)
% hObject    handle to clear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close all;
GUI1;



function display_Callback(hObject, eventdata, handles)
% hObject    handle to display (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of display as text
%        str2double(get(hObject,'String')) returns contents of display as a double


% --- Executes during object creation, after setting all properties.
function display_CreateFcn(hObject, eventdata, handles)
% hObject    handle to display (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
