
clc


%define points in the emitter frame

%load('rigid_carina_points_3');

%A = emitter frame
%B= lung frame

load regPoints.mat
% The points measured from the tracking system. Transposed for the absor
% function
A = regPoints' * 1000; % Times 1000 due to Slicer RAS in mm
% The Matrix of Fiducial points from 3D Slicer
B = F1';

[regParams,Bfit,ErrorStats]=absor(A,B) ;

R_calc=  regParams.R;
t_calc=  regParams.t;
  
Treg = regParams.M

save('data/Treg', 'Treg');

