% checkedPoints = centerLineLabeling(centerLinePoints)
% 
% Input:  centerline points, extracted from segmented CT scan.
%        
% Output: centerline points, with label assigned and changing directions 
%         allowed for every point, un to 3. Each row: [X Y Z L D1 D2 D3].
%         If no directions allowed: Di = -1.
% 
% Algorithm's Steps:
%
% 1 -  We start from one point, and assign it a label. At each iteration 
% we add to his label the nearest point. There is a threshold: when the 
% nearest point is farther than that threshold, then we start again from 
% the first point of the list (centerline points list), after having 
% removed the labeled points. Eccetera! 
% 
% 2 - Because of the structure of the algorithm, some points won't be 
% labeled: they will be labeled later, with the nearest labeled point's 
% label. 
% rline points, extracted from segmented CT scan.
%        We are assuming that .mat file contains a matrix called
%        PigCenterline.
% 
% Output: centerline points, with label assigned and changing directions 
%         allowed for every point, un to 3. Each row: [X Y Z L D1 D2 D3].
%         If no directions allowed: Di = -1.
% 
% Algorithm's steps:
%
% 1 - We start from one point, and assign it a label. At each iteration 
%     we add to his label the nearest point. There is a threshold: when the 
%     nearest point is farther than that threshold, 
% 
% 2 - We look for bifurcation points, meaning the points labeled with an 
%     index near points with another index. 

function checkedPoints = centerLineLabelingFunc(centerLinePoints)

addpath('C:\Users\Richi\Desktop\Code\Registration\mainBronchus\UI\main\funcs');

% Need to store the data, for the further iterations.

originalPoints = centerLinePoints;

centerLinePoints = unique(centerLinePoints,'rows');
centerLinePoints = sortingPoints(centerLinePoints);

% currentIndex will contain the current branch -> assigned to the points which belong to it. 
% checkedPoints will contain all of the valued points, with assigned index. Each row: XYZ+Index. 
% Initialize with first point.

currentIndex = 1;
checkedPoints = [centerLinePoints(1,1),centerLinePoints(1,2),centerLinePoints(1,3),currentIndex];
pointsToRemove = [0,0,0];

% ----------------------------------------------------------------------------------------------------------------------------------------------------------
% Algorithm.             

disp('Centerline labeling...')

tic
   
while(size(centerLinePoints,1)>=3)

    distancesFromMyPoint = pdist2(centerLinePoints(1,:),centerLinePoints(2:end,:),'euclidean');

    % Looking for the closest point.

    [minimum, minimumIndex] = min(distancesFromMyPoint);

    % In case the closest point is too far.

    if minimum > 15

        % Setting new currentIndex. The reference is the last one stored in checkedPoints.

        currentIndex = checkedPoints(end,end)+1;

        % Just a trick, it's simpler removing these points and valuing them at the end.              

        singlePointToRemove = centerLinePoints(1,:);
        pointsToRemove = [pointsToRemove;singlePointToRemove];

        % Re-loading centerline and removing points.             
        
        centerLinePoints = originalPoints;

        centerLinePoints = unique(centerLinePoints,'rows');


        centerLinePoints = centerLinePoints(~ismember(centerLinePoints,checkedPoints(:,1:3),'rows'),:);
        centerLinePoints = centerLinePoints(~ismember(centerLinePoints,pointsToRemove,'rows'),:);

        centerLinePoints = sortingPoints(centerLinePoints);

        continue

    end

    % minimumIndex+1 becasue minimum indices start from the second point.
    % The reference one is indeed the first point of the centerline
    
    nearestPoint = centerLinePoints(minimumIndex+1,:);

    % Adding the point valued at the matrix which contains the ones already 
    % values (checkedPoints).
    % It's removed from its position in centerLinePoints, and moved to the 
    % first position. -> New reference for the next iteration.

    checkedPoints = [checkedPoints; [nearestPoint,currentIndex]];
    centerLinePoints(minimumIndex+1,:) = [];
    centerLinePoints(1,:) = nearestPoint;
  
    
end

toc

% ----------------------------------------------------------------------------------------------------------------------------------------------------------

% Re-loading points to have them all.

centerLinePoints = originalPoints;

centerLinePoints = unique(centerLinePoints,'rows');

% ----------------------------------------------------------------------------------------------------------------------------------------------------------
% Handling unlabeled points.

pointsToEval = centerLinePoints(~ismember(centerLinePoints,checkedPoints(:,1:3),'rows'),:);
pointsToEval = unique(pointsToEval,'rows');

disp('Handling unlabeled points...')

for i = 1:size(pointsToEval,1)
    
    
    distancesFromMyPoint = pdist2(pointsToEval(i,:),checkedPoints(:,1:3),'euclidean');
    [minimum, minimumIndex] = min(distancesFromMyPoint);
    checkedPoints = [checkedPoints;[pointsToEval(i,:),checkedPoints(minimumIndex,4)]];

end
toc

% ----------------------------------------------------------------------------------------------------------------------------------------------------------
% Now starts the comparison between all the pairs of points: we want to
% know where the probe is allowed to change its direction.


% ----------------------------------------------------------------------------------------------------------------------------------------------------------
% Extracting bifurcation points and saving to file.             

disp('Calculating bifurcation points...')

refEuclideanDistance = 20;

% Sorting, with Z values.

checkedPoints = unique(checkedPoints,'rows');
checkedPoints = sortingPoints(checkedPoints);

% Adding columns, which will contain allowed directions values. -1 if no
% more allowed.

checkedPoints = [checkedPoints,ones(length(checkedPoints),3)*-1];

for i = 1:length(checkedPoints)

    refPoint = checkedPoints(i,1:3);
    refIndex = checkedPoints(i,4);
    
    otherPoints = checkedPoints(checkedPoints(:,4)~=refIndex,1:3);	
    otherIndices = checkedPoints(checkedPoints(:,4)~=refIndex,4);
    
    if(size(otherPoints,1)<2)
        continue
    end
    
    distancesFromRefPoint = pdist2(refPoint,otherPoints,'euclidean');
    %distancesFromRefPoint = distancesFromRefPoint(1:size(otherPoints,1));
    
    allowedDistances = find(distancesFromRefPoint<refEuclideanDistance);
    
    allowedDirections = otherIndices(allowedDistances);
    allowedDirections = unique(allowedDirections);
    
    if ~isempty(allowedDirections)
        checkedPoints(i,5:4+length(allowedDirections)) = allowedDirections;
    end
    
end
toc


% Bifurcation points.

bifurcationPoints = checkedPoints(checkedPoints(:,5)~=-1,:);
bifurcationPoints = bifurcationPoints(:,1:3);

save checkedPoints checkedPoints;