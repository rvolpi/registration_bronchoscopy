function cost = costFunc(fakeProbePoints,target)

% Calculating the squared sum of the distances between probe points and the
% closest centerline points, taking into account the three indices, 1 for
% the trachea and 2,3 for the main bronchus.

path1 = fakeProbePoints(fakeProbePoints(:,4)==1,1:3);
target1 = target(target(:,4)==1,1:3); 
path2 = fakeProbePoints(fakeProbePoints(:,4)==2,1:3);
target2 = target(target(:,4)==2,1:3);
path3 = fakeProbePoints(fakeProbePoints(:,4)==3,1:3);
target3 = target(target(:,4)==3,1:3);

% VERY IMPORTANT: I'm assuming that both sets of points are sorted wrt
% label values. So, 1 is first and 3 is last.

[distances1,closestPointsIndices1] = min(pdist2(path1,target1,'euclidean'),[],2); % Faster in this way than with dsearchn function.
closestPoints1 = target1(closestPointsIndices1,:);
[distances2,closestPointsIndices2] = min(pdist2(path2,target2,'euclidean'),[],2); % Faster in this way than with dsearchn function.
closestPoints2 = target2(closestPointsIndices2,:);
[distances3,closestPointsIndices3] = min(pdist2(path3,target3,'euclidean'),[],2); % Faster in this way than with dsearchn function.
closestPoints3 = target3(closestPointsIndices3,:);

closestPoints = [closestPoints1;closestPoints2;closestPoints3];

cost = sum(sum((closestPoints(:,1:3)-fakeProbePoints(:,1:3)).^2))/length(fakeProbePoints);


