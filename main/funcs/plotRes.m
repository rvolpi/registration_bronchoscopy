function plotRes(i,mainPoints,fakeProbePoints)

figure(i)

scatter3(mainPoints(mainPoints(:,4)==1,1),mainPoints(mainPoints(:,4)==1,2),mainPoints(mainPoints(:,4)==1,3),20,'k');
hold on
grid off
scatter3(mainPoints(mainPoints(:,4)==2,1),mainPoints(mainPoints(:,4)==2,2),mainPoints(mainPoints(:,4)==2,3),20,'k');
scatter3(mainPoints(mainPoints(:,4)==3,1),mainPoints(mainPoints(:,4)==3,2),mainPoints(mainPoints(:,4)==3,3),20,'k');

scatter3(fakeProbePoints(fakeProbePoints(:,4)==1,1),fakeProbePoints(fakeProbePoints(:,4)==1,2),fakeProbePoints(fakeProbePoints(:,4)==1,3),20,'r');
hold on
grid off
scatter3(fakeProbePoints(fakeProbePoints(:,4)==2,1),fakeProbePoints(fakeProbePoints(:,4)==2,2),fakeProbePoints(fakeProbePoints(:,4)==2,3),20,'r');
scatter3(fakeProbePoints(fakeProbePoints(:,4)==3,1),fakeProbePoints(fakeProbePoints(:,4)==3,2),fakeProbePoints(fakeProbePoints(:,4)==3,3),20,'r');

