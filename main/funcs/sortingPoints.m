function sortedPoints = sortingPoints(centerLinePoints)

    [q,w] = sort(centerLinePoints(:,3));
    sortedPoints = fliplr(centerLinePoints(w,:)')';

