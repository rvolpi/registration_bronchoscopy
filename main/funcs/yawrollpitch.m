function R = yawrollpitch(alfa,beta,gamma)

Rz = [cos(alfa),-sin(alfa),0;sin(alfa),cos(alfa),0;0,0,1];

Ry = [cos(beta),0,-sin(beta);0,1,0;sin(beta),0,cos(beta)];

Rx = [1,0,0;0,cos(gamma),-sin(gamma);0,sin(gamma),cos(gamma)];

R = Rz*Ry*Rx;