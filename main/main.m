close all, clear all, clc

addpath('.\refactored_Rev4')
addpath('.\funcs')
addpath('.\stl')

load probe

% datapath is the path of the centerline .txt file. 

% datapath = 'C:\Users\Richi\Desktop\Code\various\data\Clear_Complete_Centerline_LAST.txt';
% datapath = 'C:\Users\Richi\Desktop\Code\various\data\CenterlinePoints_Complete_ALL.txt';
% datapath = 'C:\Users\Richi\Desktop\Code\various\data\CenterlinePoints_Complete.txt';
% datapath = '.\data\Centerline_Phantom_18092015.txt';

datapath = 'c16.txt';

centerLinePoints = load(datapath);

% checkedPoints are the centerline points with labels and bifurcation 
% points. This step is needed, since labels and bif. points are required 
% in the following step.

checkedPoints = centerLineLabelingFunc(centerLinePoints);

save data1

% In this step, the balanced survey portion of the centerline is extracted,
% through a UI in which the user is required to select 4 points.

[mainPoints,mainCarinaPoint] = extractCenterline(checkedPoints);

save data2

% acquireProbeData is the function which leads to the UI to acquire the
% balanced survey data.

% % % % % [probePoints, probeCarinaPoint] = acquireProbeData();
% % % % % 
% % % % % save data3
% % % % % 
% % % % % % Scaling factor
% % % % % 
% % % % % clear,clc
% % % % % load data3
% % % % % 
% % % % % probeCarinaPoint = probeCarinaPoint*1000;
% % % % % probePoints(:,1:3) = probePoints(:,1:3)*1000;
% % % % % probePoints = probePoints(1:10:end,:);
% % % % % 
% % % % % mainPoints1 = mainPoints(mainPoints(:,4)==1,:);
% % % % % mainPoints2 = mainPoints(mainPoints(:,4)==2,:);
% % % % % mainPoints3 = mainPoints(mainPoints(:,4)==3,:);
% % % % % mainPoints = [mainPoints1(1:100:end,:);mainPoints2(1:100:end,:);mainPoints3(1:10:end,:)];

% ADD RESPIRATORY GATING HERE IF USED. SCRIPT IS READY.

% Finally, the tranformation matrix is found.


transMatrix = registrationAlg(mainPoints,mainCarinaPoint,probePoints,probeCarinaPoint);


save data4