% T_final = registrationAlg(mainPoints,mainCarinaPoint,probePoints,probeCarinaPoint)
%
% Input: mainPoints: portion of the centerline extracted through 
%        extractCenterline.m.
%
%        mainCarinaPoint: carina point of the centerline, extracted again
%        through the script extractCenterline.m.
%
%        probePoints: balanced survey, acquired through the script 
%        acquireProbeData.m
%
%        probeCarinaPoint: patient carina position, acquired adain through
%        the script acquireProbeData.m
%
% Output: transformation matrix to squitch from patient coordiate system 
%         to the virtual one. 
%
%         T_final = [r_11  r_12  r_13  t_x
%                    r_21  r_22  r_23  t_y
%                    r_31  r_32  r_33  t_z
%                     0     0     0     1]
%
%
%
% Registration with two-steps iterations (SVD-ICP wrt carina/ Sim. Ann. to translate)
% using trachea and main bronchus skeleton, extracted with the other script.
% At the end of the '2-iterations' part, a final transformation is found
% through a 6-variables (yaw,roll,pitch and x,y,z translations)
% minimizations achieved again through simulated annealing.
%
% This script assumes that centerline points are stored in a Nx4 matrix,
% where for each row X,Y,Z coordinates are the first three values, and the
% fourth value is a label index: 1 for thrachea, 1,2 for main bronchus.


function T_final = registrationAlg(mainPoints,mainCarinaPoint,probePoints,probeCarinaPoint)

addpath('./stl');

tic

% Taking less probe points, to speed up the algorithm.

probePoints = probePoints(1:1:end,:);

% Plotting initial condition.

plotRes(2,mainPoints,probePoints)

pause(0.1)

% Centering wrt carinaPoint. clc

mainPoints(:,1:3) = mainPoints(:,1:3) - repmat(mainCarinaPoint,length(mainPoints),1);
probePoints(:,1:3) = probePoints(:,1:3) - repmat(probeCarinaPoint,length(probePoints),1);

% T will be a 4x4xN matrix, where each 4x4 submatrix will cover the 
% transformation found in a step. The first one, is simply a translation to
% the origin, using the probe carina point as a center. The last one will
% be the translation wrt the virtual points carina position.

T = [eye(3),-probeCarinaPoint';0,0,0,1];

% Plotting data centered.

plotRes(3,mainPoints,probePoints)
zlim([-20,20])

pause(0.3)

%---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
% In this 'two-step iterations' part of the algorithm, a transformation in
% found. It will be used as an initial condition for the 6 variables
% minimization (yaw,roll,pitch and 3elements translation vector), but it
% turns out that the minimum found would be good enough for navigation.
%
% Two-steps iterations: 
% 
% 1) ICP with SVD wrt carina-origin.
%
% 2) Simulated Annealing to optimize translation.

refCost = 0;
newCost = 1000;

while(abs(newCost - refCost)>0.1) % The cost is provided in mm^2.

    disp(costFunc(probePoints,mainPoints))
    
    refCost = costFunc(probePoints,mainPoints);

    % -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    % Applying ICP through SVD, taking closest points only among same labels.  

    R = zeros(3);

    target = mainPoints; % I search mainPoints closest points to fakeProbePoints, and rotate fakeProbePoints to reduce mean squared error. 


    path1 = probePoints(probePoints(:,4)==1,1:3);
    target1 = target(target(:,4)==1,1:3); 
    path2 = probePoints(probePoints(:,4)==2,1:3);
    target2 = target(target(:,4)==2,1:3);
    path3 = probePoints(probePoints(:,4)==3,1:3);
    target3 = target(target(:,4)==3,1:3);
    
    counter = 0;


    while(round(sum(diag(R))*10000000)~=30000000)
       
        % VERY IMPORTANT: I'm assuming that both sets of points are sorted wrt
        % label values. So, 1 is first and 3 is last.

        [distances1,closestPointsIndices1] = min(pdist2(path1,target1,'euclidean'),[],2); % Faster in this way than with dsearchn function.
        closestPoints1 = target1(closestPointsIndices1,:);
        [distances2,closestPointsIndices2] = min(pdist2(path2,target2,'euclidean'),[],2); 
        closestPoints2 = target2(closestPointsIndices2,:);
        [distances3,closestPointsIndices3] = min(pdist2(path3,target3,'euclidean'),[],2); 
        closestPoints3 = target3(closestPointsIndices3,:);
       
        
        closestPoints = [closestPoints1;closestPoints2;closestPoints3];

        % Minimization finding algebric minimum through Kabsch algorithm.
        
        H = closestPoints(:,1:3)'*probePoints(:,1:3);

        [U,S,V] = svd(H); 

        d = sign(det(V*U'));
        
        R = V*[1,0,0;0,1,0;0,0,d]*U';
        
        probePoints(:,1:3) = probePoints(:,1:3)*R;
        path1 = path1*R;
        path2 = path2*R;
        path3 = path3*R;
        probeCarinaPoint = probeCarinaPoint*R;
        
        T(:,:,end+1) = [R',[0,0,0]';0,0,0,1];
        
    end
          
    % -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    % Adjusting translation with simulated annealing. 

    p0 = [0,0,0]; 

    [x,f] = anneal(@(p)costFunc(probePoints+repmat([p(1),p(2),p(3),0],length(probePoints),1),mainPoints),p0,struct('Verbosity',2,'StopTemp',0.1,'MaxConsRej',100000000));

    probePoints = probePoints + repmat([x(1),x(2),x(3),0],length(probePoints),1);
    probeCarinaPoint = probeCarinaPoint + [x(1),x(2),x(3)];
    
    newCost = costFunc(probePoints,mainPoints);
    
    T(:,:,end+1) = [eye(3),x';0,0,0,1];

end

% --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

disp(costFunc(probePoints,mainPoints))

% Final minimization: simulated annealing with yaw,roll,pitch and translations.

disp('FINAL MINIMIZATION!')

p0 = [0,0,0,0,0,0]; 

[x,f] = anneal(@(p)costFunc([probePoints(:,1:3)*yawrollpitch(p(4),p(5),p(6))+repmat([p(1),p(2),p(3)],length(probePoints),1),probePoints(:,4)],mainPoints),p0,struct('Verbosity',2,'StopTemp',0.000001,'MaxConsRej',100000000));

probePoints(:,1:3) = probePoints(:,1:3)*yawrollpitch(x(4),x(5),x(6)) + repmat([x(1),x(2),x(3)],length(probePoints),1);
probeCarinaPoint = probeCarinaPoint(1:3)*yawrollpitch(x(4),x(5),x(6)) + [x(1),x(2),x(3)];

T(:,:,end+1) = [yawrollpitch(x(4),x(5),x(6)),[x(1);x(2);x(3)];0,0,0,1];
T(:,:,end+1) = [eye(3),mainCarinaPoint';0,0,0,1];

% In practice, the transformed point will be found simply in this way:
% virtualPoint = T_final*probePoint, with both points stored in homogeneous
% cordinates (x,y,z,1)'.

T_final = eye(4,4);

for q = 0:size(T,3)-1
   
    T_final = T_final*T(:,:,end-q);
    
end

newCost = costFunc(probePoints,mainPoints);

disp(costFunc(probePoints,mainPoints))

%Plotting results.

plotRes(4,mainPoints,probePoints)
xlim([-100,100])
ylim([-100,100])
zlim([-20,20])

figure(5)
probePoints(:,1:3) = probePoints(:,1:3) + repmat(mainCarinaPoint,length(probePoints),1);
scatter3(probePoints(:,1),probePoints(:,2),probePoints(:,3),20,'r')
fv = stlread('PerspexModelHalf.stl');
%% Render
% The model is rendered with a PATCH graphics object. We also add some dynamic
% lighting, and adjust the material properties to change the specular
% highlighting.
hold on
patch(fv,'FaceColor',       [0.8 0.8 1.0], ...
         'EdgeColor',       'none',        ...
         'FaceLighting',    'gouraud',     ...
         'AmbientStrength', 0.15);
% Add a camera light, and tone down the specular highlighting
camlight('headlight');
material('dull');
% Fix the axes scaling, and set a nice view angle
axis('image');
view([-135 35]);

save forinpoly




toc
